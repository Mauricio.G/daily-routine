<?php
 
namespace app\controllers;
use Yii;
use yii\data\SqlDataProvider;
class RelatoriosController extends \yii\web\Controller
{
   public function actionIndex()
   {
       return $this->render('index');
   }
 

public function actionRelatorio1()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT DATA, COUNT(*)
        FROM mt_EVENTOS
        GROUP BY DATA',
            ]
        );
        
        return $this->render('relatorio1', ['resultado' => $consulta]);
   }


   public function actionRelatorio2()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT H_INICIAL, COUNT(*)
        FROM mt_EVENTOS
        GROUP BY H_INICIAL',
            ]
        );
        
        return $this->render('relatorio2', ['resultado' => $consulta]);
   }


   public function actionRelatorio3()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT H_FINAL, COUNT(*)
        FROM mt_EVENTOS
        GROUP BY H_FINAL',
            ]
        );
        
        return $this->render('relatorio3', ['resultado' => $consulta]);
   }

   public function actionRelatorio4()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT SOM_ID, COUNT(*)
        FROM mt_EVENTOS
        GROUP BY SOM_ID',
            ]
        );
        
        return $this->render('relatorio4', ['resultado' => $consulta]);
   }

   public function actionRelatorio5()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT ALARME, COUNT(*)
        FROM mt_EVENTOS
        GROUP BY ALARME',
            ]
        );
        
        return $this->render('relatorio5', ['resultado' => $consulta]);
   }

}
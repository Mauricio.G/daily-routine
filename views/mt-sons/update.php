<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MtSons */

$this->title = 'Update Mt Sons: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Mt Sons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mt-sons-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

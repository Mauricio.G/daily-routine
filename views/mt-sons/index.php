<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MtSonsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mt-sons-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Criar Sons', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'NOME',
            'MODO',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

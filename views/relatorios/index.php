<?php
 
use yii\helpers\Html;
 
$this->title = 'Relatórios';
$this->params['breadcrumbs'][] = $this->title;
?>
 
<div class="relatorios-index">
 
   <h2><?= Html::encode($this->title) ?></h2>
   <?= Html::a('Quantidade de Eventos por dia', ['relatorio1'], ['class' => 'btn btn-success']) ?>
   
   <h2><?= Html::encode($this->title) ?></h2>
   <?= Html::a('Quantidade de Hora Inicial', ['relatorio2'], ['class' => 'btn btn-success']) ?>

   <h2><?= Html::encode($this->title) ?></h2>
   <?= Html::a('Quantidade de Hora Final', ['relatorio3'], ['class' => 'btn btn-success']) ?>

   <h2><?= Html::encode($this->title) ?></h2>
   <?= Html::a('Quantidade de Sons', ['relatorio4'], ['class' => 'btn btn-success']) ?>

   <h2><?= Html::encode($this->title) ?></h2>
   <?= Html::a('Alarme prediletos', ['relatorio5'], ['class' => 'btn btn-success']) ?>
</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MtEventosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Eventos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mt-eventos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Criar Eventos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'DESCRICAO',
            'H_INICIAL',
            'H_Final',
            'DATA',
            //'ALARME',
            //'USUARIO_ID',
            //'SOM_ID',
            //'CATEGORIA_ID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

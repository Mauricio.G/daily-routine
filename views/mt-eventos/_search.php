<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MtEventosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mt-eventos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'DESCRICAO') ?>

    <?= $form->field($model, 'H_INICIAL') ?>

    <?= $form->field($model, 'H_Final') ?>

    <?= $form->field($model, 'DATA') ?>

    <?php // echo $form->field($model, 'ALARME') ?>

    <?php // echo $form->field($model, 'USUARIO_ID') ?>

    <?php // echo $form->field($model, 'SOM_ID') ?>

    <?php // echo $form->field($model, 'CATEGORIA_ID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

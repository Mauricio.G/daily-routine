<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MtEventos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="eventos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'DESCRICAO')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'H_INICIAL')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'H_Final')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DATA')->textInput() ?>

    <?= $form->field($model, 'ALARME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'USUARIO_ID')->textInput() ?>

    <?= $form->field($model, 'SOM_ID')->textInput() ?>

    <?= $form->field($model, 'CATEGORIA_ID')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

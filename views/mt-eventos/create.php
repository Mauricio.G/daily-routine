<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MtEventos */

$this->title = 'Criar Eventos';
$this->params['breadcrumbs'][] = ['label' => ' Eventos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Criar Eventos">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

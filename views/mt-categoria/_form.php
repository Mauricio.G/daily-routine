<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MtCategoria */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categoria">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'DESCRICAO')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

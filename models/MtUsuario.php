<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mt_usuario".
 *
 * @property int $ID
 * @property string $NOME
 * @property string $EMAIL
 * @property string $SENHA
 *
 * @property MtEventos[] $mtEventos
 */
class MtUsuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mt_usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NOME', 'EMAIL'], 'string', 'max' => 25],
            [['SENHA'], 'string', 'max' => 12],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'NOME' => 'Nome',
            'EMAIL' => 'Email',
            'SENHA' => 'Senha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtEventos()
    {
        return $this->hasMany(MtEventos::className(), ['USUARIO_ID' => 'ID']);
    }
}

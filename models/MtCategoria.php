<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mt_categoria".
 *
 * @property int $ID
 * @property string $DESCRICAO
 *
 * @property MtEventos[] $mtEventos
 */
class MtCategoria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mt_categoria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DESCRICAO'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'DESCRICAO' => 'Descrição',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtEventos()
    {
        return $this->hasMany(MtEventos::className(), ['CATEGORIA_ID' => 'ID']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mt_sons".
 *
 * @property int $ID
 * @property string $NOME
 * @property string $MODO
 *
 * @property MtEventos[] $mtEventos
 */
class MtSons extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mt_sons';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NOME'], 'string', 'max' => 25],
            [['MODO'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'NOME' => 'Nome',
            'MODO' => 'Modo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtEventos()
    {
        return $this->hasMany(MtEventos::className(), ['SOM_ID' => 'ID']);
    }
}

<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MtEventos;

/**
 * MtEventosSearch represents the model behind the search form of `app\models\MtEventos`.
 */
class MtEventosSearch extends MtEventos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'USUARIO_ID', 'SOM_ID', 'CATEGORIA_ID'], 'integer'],
            [['DESCRICAO', 'H_INICIAL', 'H_Final', 'DATA', 'ALARME'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MtEventos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'DATA' => $this->DATA,
            'USUARIO_ID' => $this->USUARIO_ID,
            'SOM_ID' => $this->SOM_ID,
            'CATEGORIA_ID' => $this->CATEGORIA_ID,
        ]);

        $query->andFilterWhere(['like', 'DESCRICAO', $this->DESCRICAO])
            ->andFilterWhere(['like', 'H_INICIAL', $this->H_INICIAL])
            ->andFilterWhere(['like', 'H_Final', $this->H_Final])
            ->andFilterWhere(['like', 'ALARME', $this->ALARME]);

        return $dataProvider;
    }
}

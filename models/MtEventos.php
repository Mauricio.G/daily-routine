<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mt_eventos".
 *
 * @property int $ID
 * @property string $DESCRICAO
 * @property string $H_INICIAL
 * @property string $H_Final
 * @property string $DATA
 * @property string $ALARME
 * @property int $USUARIO_ID
 * @property int $SOM_ID
 * @property int $CATEGORIA_ID
 *
 * @property MtUsuario $uSUARIO
 * @property MtSons $sOM
 * @property MtCategoria $cATEGORIA
 */
class MtEventos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mt_eventos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DATA'], 'safe'],
            [['USUARIO_ID', 'SOM_ID', 'CATEGORIA_ID'], 'integer'],
            [['DESCRICAO'], 'string', 'max' => 25],
            [['H_INICIAL', 'H_Final', 'ALARME'], 'string', 'max' => 12],
            [['USUARIO_ID'], 'exist', 'skipOnError' => true, 'targetClass' => MtUsuario::className(), 'targetAttribute' => ['USUARIO_ID' => 'ID']],
            [['SOM_ID'], 'exist', 'skipOnError' => true, 'targetClass' => MtSons::className(), 'targetAttribute' => ['SOM_ID' => 'ID']],
            [['CATEGORIA_ID'], 'exist', 'skipOnError' => true, 'targetClass' => MtCategoria::className(), 'targetAttribute' => ['CATEGORIA_ID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'DESCRICAO' => 'Descrição',
            'H_INICIAL' => 'Hora Inicial',
            'H_Final' => 'Hora Final',
            'DATA' => 'Data',
            'ALARME' => 'Alarme',
            'USUARIO_ID' => 'Usuario ',
            'SOM_ID' => 'Som ',
            'CATEGORIA_ID' => 'Categoria ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSUARIO()
    {
        return $this->hasOne(MtUsuario::className(), ['ID' => 'USUARIO_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSOM()
    {
        return $this->hasOne(MtSons::className(), ['ID' => 'SOM_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCATEGORIA()
    {
        return $this->hasOne(MtCategoria::className(), ['ID' => 'CATEGORIA_ID']);
    }
}
